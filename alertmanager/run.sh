#!/bin/bash

CFG_DIR="/opt/alertmanager/conf"

# replace env vars in cfg

/opt/alertmanager/alertmanager --config.file=$CFG_DIR/alertmanager.yml 

./alertmanager --web.listen-address=:9093
