# Info

This was built from https://github.com/prometheus/client_golang.git at version 2.12.0

I went and made an image and through it in a public ecr 

```public.ecr.aws/k2k7h6n7/golang-client-example-docker:latest```

You can run a prometheus client example ( sample or random ) by passing ./random as an argument
