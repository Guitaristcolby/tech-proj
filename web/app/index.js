'use strict';

const express = require('express');
const request = require("request-promise");
const process = require('process')

// Constants
const PORT = process.env.NODE_PORT;
const HOST = '0.0.0.0';

// App
const client = require('prom-client');
const collectDefaultMetrics = client.collectDefaultMetrics;
// Probe every 5th second.
collectDefaultMetrics({ timeout: 5000 });

const counter = new client.Counter({
  name: 'node_request_operations_total',
  help: 'The total number of processed requests'
});

const histogram = new client.Histogram({
  name: 'node_request_duration_seconds',
  help: 'Histogram for the duration in seconds.',
  buckets: [1, 2, 5, 6, 10]
});

const app = express();
app.get('/', (req, res) => {

  //Simulate a sleep
  var start = new Date()
  var simulateTime = 1000

  setTimeout(function(argument) {
    // execution time simulated with setTimeout function
    var end = new Date() - start
    histogram.observe(end / 1000); //convert to seconds
  }, simulateTime)

  counter.inc();
  
  res.send('Hello world\n');
});

app.get('/shutdown', (req, res) => {
  process.exit(0); 
});


// Metrics endpoint
app.get('/metrics', (req, res) => {
  res.set('Content-Type', client.register.contentType)
  res.end(client.register.metrics())
})

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);


function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

const options = {
  method: 'GET',
  uri: `http://${HOST}:${PORT}`
}

async function test(shutdown) {
  if(shutdown === "true") {
    console.log('Testing shutdown to trigger alert in 30 seconds');
    await sleep(30000);
    process.exit(0); 
  } else {
    while(true) {
      // console.log("Testing requests");
      request(options)
      await sleep(1);
    }
  }
}

module.exports = {
  test
};

