# README

## Overview

In this repository are the barebone elements of a simple Grafana/Prometheus/AlertManager stack, with a DockerCompose file.
It will need some extra configuration but hopefully not too much. 
We've grabbed a directory structure, some example conf files, Dockerfiles and a docker-compose file.

This was all done on our 2018 MBPs.

As you go through this you will find something that seems like there is an underlying issue that's an unexpected blocker. 
Expect it. You don't have to fix the underlying issue to complete the objectives. 

## Resources

Prometheus queries - https://prometheus.io/docs/prometheus/latest/querying/basics/

Alertmanager - https://prometheus.io/docs/alerting/latest/alertmanager/

Robust Perception - https://www.robustperception.io/blog  

Local Grafana - http://localhost:3000

Local Prom - http://localhost:9090

Local Alertmanager - http://localhost:9093

Client metrics are available at the path /metrics. Port mappings are in the composefile.

## Objectives

1. Launch Prometheus/Grafana/Alertmanger so that they provide a working Promstack using docker-compose.
2. Add an additional target for prometheus to monitor ( or multiples if you would like ).  *Hint/Tip* look at the docker-compose.yml.
3. Create at least 2 additional rules which can be triggered.(e.g. you can trigger a node down, maybe be tougher to trigger a memory leak. 
4. Rule violations should be sent to Alertmanager.
5. The `Underutilized fds` is written wrong. Fix it as you think is appropriate.
6. One of the other alerts is poorly written and has unexpected behavior. Find it and fix it however you want.
7. Make an interesting dashboard for the metrics you're collecting. There's no "right" answer, have some fun.
8. Using any metric, create an alert that fires if the metric has changed over the past 5 minutes more than the change of the previous 5 minutes.
9. Check everything into your branch and then push it.
10. When we run docker-compose it comes up.
X. FIN!

## Notes

* If you want to do more things, please do. Cool graphs, add some plugins, I encourage you to express yourself.
* If just those objectives are finished, and you've applied the level of rigor/documentation/polish that you're satisfied with, you won't be penalized for not doing more. 
* You can use `promtool` and `amtool` to check config files. These are included in the releases so you would have to grab that. 
* Life happens, things happen, if this technical assignment isn't working, please let us know ASAP. We'll find something that does work.
